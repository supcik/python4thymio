__all__ = ["tstr", "int16", "parse16", "s12", "s16"]


def tstr(x: str) -> bytearray:
    t = bytes(str.encode(x))
    return bytearray([len(t)]) + t


def int16(x: int) -> bytearray:
    return bytearray([(x >> i & 0xff) for i in (0, 8)])


def parse16(x: bytearray, i=0):
    return int(x[i]) + (int(x[i+1]) << 8)


def s12(x):
    if x >= (1 << 11):
        return x - (1 << 12)
    else:
        return x


def s16(x):
    if x >= (1 << 15):
        return x - (1 << 16)
    else:
        return x