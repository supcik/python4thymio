import unittest
import thymio.message as tmsg


class TestMessages(unittest.TestCase):
    def setUp(self):
        pass

    def test_list_nodes(self):
        m = tmsg.ListNodes()
        self.assertEqual(m.serialize().hex(), "0200000011a00500")
        self.assertEqual(tmsg.parse_message(m.serialize()).serialize().hex(), "0200000011a00500")

    def test_bootloader_reset(self):
        m = tmsg.BootloaderReset(dest=300)
        self.assertEqual(m.serialize().hex(), "0200000000802c01")
        self.assertEqual(tmsg.parse_message(m.serialize()).serialize().hex(), "0200000000802c01")

    def test_bootloader_read_page(self):
        m = tmsg.BootloaderReadPage(dest=2000, page_number=2)
        self.assertEqual(m.serialize().hex(), "040000000180d0070200")
        self.assertEqual(tmsg.parse_message(m.serialize()).serialize().hex(), "040000000180d0070200")

    def test_bootloader_page_data_write(self):
        m = tmsg.BootloaderPageDataWrite(dest=10, data=b"aaaaa")
        m.dest = 20
        self.assertEqual(m.serialize().hex(), "07000000038014006161616161")
        self.assertEqual(tmsg.parse_message(m.serialize()).serialize().hex(), "07000000038014006161616161")

    def test_bootloader_ack(self):
        m = tmsg.BootloaderAck(src=137, error_code=2, error_address=12)
        self.assertEqual(m.serialize().hex(), "04008900068002000c00")
        self.assertEqual(tmsg.parse_message(m.serialize()).serialize().hex(), "04008900068002000c00")


    def test_bootloader_ack_without_address(self):
        m = tmsg.BootloaderAck(error_code=2)
        self.assertEqual(m.serialize().hex(), "0200000006800200")
        self.assertEqual(tmsg.parse_message(m.serialize()).serialize().hex(), "0200000006800200")


    def test_named_variable_description(self):
        m = tmsg.NamedVariableDescription(size=12, name="Humanoïd")
        self.assertEqual(m.serialize().hex(), "0c00000001900c000948756d616e6fc3af64")
        self.assertEqual(tmsg.parse_message(m.serialize()).serialize().hex(), "0c00000001900c000948756d616e6fc3af64")


    def test_disconnected(self):
        m = tmsg.Disconnected()
        self.assertEqual(m.serialize().hex(), "000000000490")
        self.assertEqual(tmsg.parse_message(m.serialize()).serialize().hex(), "000000000490")

    def test_variables(self):
        m = tmsg.Variables(start=1, variable=[1,2,3,4,5])
        self.assertEqual(m.serialize().hex(), "0c0000000590010001000200030004000500")
        self.assertEqual(tmsg.parse_message(m.serialize()).serialize().hex(), "0c0000000590010001000200030004000500")


    def test_native_function_description(self):
        m = tmsg.NativeFunctionDescription(
            name="Androïd", description="Smartphone", parameter_length=4,
            parameter=[
                {'size': 4, 'name': "Param1"},
                {'size': 4, 'name': "Param2"},
                {'size': 4, 'name': "Param3"},
                {'size': 4, 'name': "Param4"}
            ])
        self.assertEqual(
            m.serialize().hex(),
            "3a000000039008416e64726fc3af640a536d61727470686f6e65040004000650"
            "6172616d31040006506172616d32040006506172616d33040006506172616d34")
        self.assertEqual(tmsg.parse_message(m.serialize()).serialize().hex(),
             "3a000000039008416e64726fc3af640a536d61727470686f6e65040004000650"
             "6172616d31040006506172616d32040006506172616d33040006506172616d34")

        self.assertEqual(m.__class__.__name__, "NativeFunctionDescription")
        self.assertEqual(hex(m.typ), "0x9003")

    def test_user_messages(self):
        m = tmsg.UserMessage(src=10, typ=169)
        self.assertEqual(m.serialize().hex(), "00000a00a900")
        self.assertEqual(tmsg.parse_message(m.serialize()).serialize().hex(), "00000a00a900")


    def test_user_messages_with_data(self):
        m = tmsg.UserMessage(src=10, typ=170, data=b'blabla')
        self.assertEqual(m.serialize().hex(), "06000a00aa00626c61626c61")
        self.assertEqual(tmsg.parse_message(m.serialize()).serialize().hex(), "06000a00aa00626c61626c61")
        self.assertEqual(m.__class__.__name__, "UserMessage")
        self.assertEqual(m.typ, 170)

if __name__ == '__main__':
    unittest.main()