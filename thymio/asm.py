from thymio.util import *

_simple_instructions = [
    ("Stop", 0x0),
    ("Return", 0xe),
]

_memory_move_instructions = [
    ("Load", 0x3),
    ("Store", 0x4),
]

_memory_move_indirect_instructions = [
    ("LoadIndirect", 0x5),
    ("StoreIndirect", 0x6),
]

_arithmetic_instructions = [
    ("Negate", 0x7, 0x00),
    ("Absolute", 0x7, 0x01),
    ("BitNot", 0x7, 0x02),
    ("ShiftLeft", 0x8, 0x00),
    ("ShiftRight", 0x8, 0x01),
    ("Add", 0x8, 0x02),
    ("Subtract", 0x8, 0x03),
    ("Multiply", 0x8, 0x04),
    ("Divide", 0x8, 0x05),
    ("Modulo", 0x8, 0x06),
    ("BitOr", 0x8, 0x07),
    ("BitExclusiveOr", 0x8, 0x08),
    ("BitAnd", 0x8, 0x09),
    ("Equal", 0x8, 0x0A),
    ("NotEqual", 0x8, 0x0B),
    ("BiggerThan", 0x8, 0x0C),
    ("BiggerEqualThan", 0x8, 0x0D),
    ("SmallerThan", 0x8, 0x0E),
    ("SmallerEqualThan", 0x8, 0x0F),
    ("LogicalOr", 0x8, 0x10),
    ("LogicalAnd", 0x8, 0x11),
]

_branch_instructions = [
    ("ShiftLeft", 0x00),
    ("ShiftRight", 0x01),
    ("Add", 0x02),
    ("Subtract", 0x03),
    ("Multiply", 0x04),
    ("Divide", 0x05),
    ("Modulo", 0x06),
    ("BitOr", 0x07),
    ("BitAnd", 0x08),
    ("Equal", 0x09),
    ("NotEqual", 0x0A),
    ("Bigger", 0x0B),
    ("BiggerOrEqual", 0x0C),
    ("Smaller", 0x0D),
    ("SmallerOrEqual", 0x0E),
    ("LogicalOr", 0x0F),
    ("LogicalAnd", 0x10),
]


class Instruction:
    def __init__(self):
        self._i_size = 1
        self.pc = None  # for __repr___

    def i_size(self):
        return self._i_size

    def disassemble(self, code: bytearray):
        pass


class Push(Instruction):
    def __init__(self, val=None):
        Instruction.__init__(self)
        self.val = val

    def __repr__(self) -> str:
        return "{0} {1}".format(self.__class__.__name__, self.val)

    def i_size(self):
        if -2048 <= self.val < 2048:
            return 1
        else:
            return 2

    def emit(self) -> bytearray:
        if self.i_size() == 1:
            return int16((0x1 << 12) + (self.val & 0x0FFF))
        else:
            return int16(0x2 << 12) + int16(self.val & 0xFFFF)

    def disassemble(self, code: bytearray):
        i = parse16(code)
        if (i & 0xF000) >> 12 == 0x1:
            self.val = s12(i & 0x0FFF)
        else:
            self.val = s16(parse16(code, 2))


class Jump(Instruction):
    def __init__(self, disp=None):
        Instruction.__init__(self)
        self.disp = disp

    def __repr__(self) -> str:
        res = "{0} {1}".format(self.__class__.__name__, self.disp)
        if self.pc is not None:
            res += " # -> {0:04X}".format(self.pc + self.disp)
        return res

    def emit(self) -> bytearray:
        return int16((0x9 << 12) + (self.disp & 0x0FFF))

    def disassemble(self, code: bytearray):
        self.disp = s12(parse16(code) & 0x0FFF)


class Emit(Instruction):
    def __init__(self, event=None, array=None, size=None):
        Instruction.__init__(self)
        self.event = event
        self.array = array
        self.size = size
        self._i_size = 3

    def __repr__(self) -> str:
        return "{0} {1} {2} {3}".format(self.__class__.__name__, self.event, self.array, self.size)

    def emit(self) -> bytearray:
        return int16((0xB << 12) + (self.event & 0x0FFF)) + int16(self.array & 0xFFFF) + int16(self.size & 0xFFFF)

    def disassemble(self, code: bytearray):
        self.event = parse16(code) & 0x0FFF
        self.array = parse16(code, 2)
        self.size = parse16(code, 4)


class NativeCall(Instruction):
    def __init__(self, id=None):
        Instruction.__init__(self)
        self.id = id

    def __repr__(self) -> str:
        return "{0} {1}".format(self.__class__.__name__, self.id)

    def emit(self) -> bytearray:
        return int16((0xC << 12) + (self.id & 0x0FFF))

    def disassemble(self, code: bytearray):
        self.id = s12(parse16(code) & 0x0FFF)


class SubroutineCall(Instruction):
    def __init__(self, dest=None):
        Instruction.__init__(self)
        self.dest = dest

    def __repr__(self) -> str:
        return "{0} {1}".format(self.__class__.__name__, self.dest)

    def emit(self) -> bytearray:
        return int16((0xD << 12) + (self.dest & 0x0FFF))

    def disassemble(self, code: bytearray):
        self.dest = s12(parse16(code) & 0x0FFF)


_diskey = {
    0x1: "Push",
    0x2: "Push",
    0x9: "Jump",
    0xB: "Emit",
    0xC: "NativeCall",
    0xD: "SubroutineCall",
}


def simple_instruction_factory(name, code):
    def __init__(self):
        Instruction.__init__(self)

    def __repr__(self) -> str:
        return "{0}".format(name)

    def emit(self) -> bytearray:
        return int16(code << 12)

    _diskey[code] = name
    return type(name, (Instruction,), {
        "__init__": __init__,
        "__repr__": __repr__,
        "emit": emit,
    })


def memory_move_instruction_factory(name, code):
    def __init__(self, index=None):
        Instruction.__init__(self)
        self.index = index

    def __repr__(self) -> str:
        return "{0} {1}".format(name, self.index)

    def emit(self) -> bytearray:
        return int16((code << 12) + (self.index & 0x0FFF))

    def disassemble(self, code: bytearray):
        self.index = s12(parse16(code) & 0x0FFF)

    _diskey[code] = name
    return type(name, (Instruction,), {
        "__init__": __init__,
        "__repr__": __repr__,
        "emit": emit,
        "disassemble": disassemble,

    })


def memory_move_indirect_instruction_factory(name, code):
    def __init__(self, array=None, size=None):
        Instruction.__init__(self)
        self.array = array
        self.size = size
        self._i_size = 2

    def __repr__(self) -> str:
        return "{0} {1}, {2}".format(name, self.array, self.size)

    def emit(self) -> bytearray:
        return int16((code << 12) + (self.array & 0x0FFF)) + int16(self.size & 0xFFFF)

    def disassemble(self, code: bytearray):
        self.array = parse16(code) & 0x0FFF
        self.size = parse16(code, 2)

    _diskey[code] = name
    return type(name, (Instruction,), {
        "__init__": __init__,
        "__repr__": __repr__,
        "emit": emit,
        "disassemble": disassemble,
    })


def arithmetic_instruction_factory(name, code, op):
    def __init__(self):
        Instruction.__init__(self)

    def __repr__(self) -> str:
        return "{0}".format(name)

    def emit(self) -> bytearray:
        return int16((code << 12) + (op & 0x00FF))

    _diskey[(code << 12) + (op & 0x00FF)] = name
    return type(name, (Instruction,), {
        "__init__": __init__,
        "__repr__": __repr__,
        "emit": emit,
    })


def branch_instruction_factory(name, code, when=False):
    def __init__(self, disp=None, t=False):
        Instruction.__init__(self)
        self.disp = disp
        self.t = t
        self._i_size = 2

    def __repr__(self) -> str:
        res = "{0}".format(name)
        if when and self.t:
            res += " T"
        res += " {0:+}".format(self.disp)
        if self.pc is not None:
            res += " # -> {0:04X}".format(self.pc + self.disp)
        return res

    def emit(self) -> bytearray:
        op = (0xA << 12)
        if when:
            op = op + (1 << 8)
            if self.t:
                op = op + (1 << 9)
        op = op + (code & 0x00FF)
        return int16(op) + int16(self.disp & 0xFFFF)

    def disassemble(self, code: bytearray):
        self.t = parse16(code) & (1 << 9) != 0
        self.disp = s16(parse16(code, 2))

    _diskey[(0XA << 12) + ((1 << 8) if when else 0) + (code & 0x00FF)] = name
    return type(name, (Instruction,), {
        "__init__": __init__,
        "__repr__": __repr__,
        "emit": emit,
        "disassemble": disassemble,
    })


def disassembled(code: bytearray):
    pc = 0
    res = list()
    while pc * 2 < len(code):
        instr = parse16(code, pc * 2)
        op = (instr & 0xF000) >> 12
        if op in (0x7, 0x8):
            op = (instr & 0xF0FF)
        elif op == 0xA:
            op = (instr & 0xF1FF)
        name = _diskey[op]
        i_class = globals()[name]
        i_obj = i_class()

        i_obj.disassemble(code[pc * 2:])
        res.append(i_obj)
        pc = pc + i_obj.i_size()

    return res


def init():
    for (name, code) in _simple_instructions:
        globals()[name] = simple_instruction_factory(name, code)
    for (name, code) in _memory_move_instructions:
        globals()[name] = memory_move_instruction_factory(name, code)
    for (name, code) in _memory_move_indirect_instructions:
        globals()[name] = memory_move_indirect_instruction_factory(name, code)
    for (name, code, op) in _arithmetic_instructions:
        globals()[name] = arithmetic_instruction_factory(name, code, op)
    for (name, code) in _branch_instructions:
        full_name = "BranchIf{0}".format(name)
        globals()[full_name] = branch_instruction_factory(full_name, code, when=False)
        full_name = "BranchWhen{0}".format(name)
        globals()[full_name] = branch_instruction_factory(full_name, code, when=True)


init()

