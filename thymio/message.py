import re
from thymio.util import *

message_types = {
    # Messages from bootloader control program to a specic node
    0x8000: ('BootloaderReset', "dest:i"),
    0x8001: ('BootloaderReadPage', "dest:i,page_number:i"),
    0x8002: ('BootloaderWritePage', "dest:i,page_number:i"),
    0x8003: ('BootloaderPageDataWrite', "dest:i,data:b"),
    # Messages from node to bootloader control program
    0x8004: ('BootloaderDescription', "page_size:i,pages_start:i,pages_count:i"),
    0x8005: ('BootloaderPageDataRead', "data:b"),
    0x8006: ('BootloaderAck', "error_code:i,error_address:i?"),
    # Messages from a specific node
    0x9000: ('Description', "name:s,protocol_version:i,bytecode_size:i,stacksize:i,"
                            "variables_size:i,named_variable_size:i,local_event_size:i,native_function_size:i"),
    0x9001: ('NamedVariableDescription', "size:i,name:s"),
    0x9002: ('LocalEventDescription', "name:s,description:s"),
    0x9003: ('NativeFunctionDescription', "name:s,description:s,parameter_length:i,parameter:[]{size:i;name:s}"),
    0x9004: ('Disconnected', ""),
    0x9005: ('Variables', "start:i,variable:[]i"),
    0x9006: ('ArrayAccessOutOfBounds', "pc:i, size:i, index:i"),
    0x9007: ('DivisionByZero', "pc:i"),
    0x9008: ('EventExecutionKilled', "pc:i"),
    0x9009: ('NodeSpecificError', "pc:i,message:s"),
    0x900A: ('ExecutionStateChanged', "pc:i,flags:i"),
    0x900B: ('BreakpointSetResult', "pc:i,success:i"),
    0x900C: ('NodePresent', "protocol_version:i"),
    # Message from IDE to all nodes
    0xA000: ('GetDescription', "version:i(5)"),
    0xA011: ('ListNodes', "version:i(5)"),
    # Messages from IDE to a specific node
    0xA001: ('SetBytecode', "dest:i,start:i,bytecode:[]i"),
    0xA002: ('Reset', "dest:i"),
    0xA003: ('Run', "dest:i"),
    0xA004: ('Pause', "dest:i"),
    0xA005: ('Step', "dest:i"),
    0xA006: ('Stop', "dest:i"),
    0xA007: ('GetExecutionState', "dest:i"),
    0xA008: ('BreakpointSet', "dest:i,pc:i"),
    0xA009: ('BreakpointClear', "dest:i,pc:i"),
    0xA00A: ('BreakpointClearAll', "dest:i"),
    0xA00B: ('GetVariables', "dest:i,start:i,length:i"),
    0xA00C: ('SetVariables', "dest:i,start:i,variable:[]i"),
    0xA00D: ('WriteBytecode', "dest:i"),
    0xA00E: ('Reboot', "dest:i"),
    0xA00F: ('SuspendToRam', "dest:i"),
    0xA010: ('GetNodeDescription', "dest:i,version:i(5)"),
    0xFFFF: ('Invalid', ""),
    # User Messages (0x0000 - 0x7FFF)
    0x0000: ('UserMessage', "data:b?"),
}


class Message:
    def __init__(self, typ=0, src=0):
        self.typ = typ
        self.src = src
        self.body = None

    def serialize_body(self):
        self.body = bytearray()

    def serialize(self):
        self.serialize_body()
        ser = bytearray()
        l = len(self.body)
        ser.extend(int16(l))
        ser.extend(int16(self.src))
        ser.extend(int16(self.typ))
        ser.extend(self.body)
        return ser


def class_factory(name, msg_typ, attributes):

    if attributes == "":
        attr_list = []
    else:
        attr_list = [tuple(i.split(":", 1)) for i in attributes.split(",")]

    def __init__(self, typ=msg_typ, src=0, **kwargs):
        Message.__init__(self, typ, src=src)

        for key, value in attr_list:
            if key in kwargs:
                setattr(self, key, kwargs[key])
                del(kwargs[key])
            else:
                default = None
                match = re.search(r'(\w)\((.*)\)', value)
                if match:
                    if match.group(1).startswith("i"):
                        default = int(match.group(2))
                    else:
                        default = match.group(2)

                setattr(self, key, default)

        if len(kwargs) != 0:
            raise TypeError("Argument(s) '%s' not valid for %s" % (", ".join(kwargs.keys()), self.__class__.__name__))

    def serialize_body(self):
        self.body = bytearray()
        for attr_name, attr_type in attr_list:  # type: str, str
            if attr_type.endswith("?") and getattr(self, attr_name) is None:
                continue
            if attr_type.startswith("i"):
                self.body.extend(int16(getattr(self, attr_name)))
            elif attr_type.startswith("b"):
                self.body.extend(bytearray(getattr(self, attr_name)))
            elif attr_type.startswith("s"):
                self.body.extend(tstr(getattr(self, attr_name)))
            elif attr_type.startswith("[]i"):
                for i in getattr(self, attr_name):
                    self.body.extend(int16(i))
            elif attr_type.startswith("[]{"):
                subtype = [tuple(i.split(":")) for i in attr_type[3:-1].split(";")]
                for i in getattr(self, attr_name):
                    for sub_attr_name, sub_attr_type in subtype:
                        if sub_attr_type.startswith("i"):
                            self.body.extend(int16(i[sub_attr_name]))
                        elif sub_attr_type.startswith("s"):
                            self.body.extend(tstr(i[sub_attr_name]))
                        else:
                            raise TypeError("{0} : Unknown type".format(sub_attr_type))

            else:
                raise TypeError("{0} : Unknown type".format(attr_type))

    def parse(self, data: bytearray):
        l = parse16(data[0:2])
        src = parse16(data[2:4])
        typ = parse16(data[4:6])
        body = data[6:]

        if len(body) != l:
            raise TypeError("invalid length")

        if typ != self.typ:
            raise TypeError("invalid type")

        if src != self.src:
            raise TypeError("invalid source")

        for attr_name, attr_type in attr_list:  # type: str, str
            if attr_type.endswith("?") and len(body) == 0:
                continue
            if attr_type.startswith("i"):
                setattr(self, attr_name, parse16(body[0:2]))
                body = body[2:]
            elif attr_type.startswith("b"):
                setattr(self, attr_name, body)
                body = bytearray()
            elif attr_type.startswith("s"):
                slen = int(body[0])
                setattr(self, attr_name, body[1:slen+1].decode("utf-8"))
                body = body[slen+1:]
            elif attr_type.startswith("[]i"):
                l = list()
                while len(body) > 0:
                    l.append(parse16(body[0:2]))
                    body = body[2:]
                    setattr(self, attr_name, l)
            elif attr_type.startswith("[]{"):
                l = list()
                subtype = [tuple(i.split(":")) for i in attr_type[3:-1].split(";")]
                while len(body) > 0:
                    o = dict()
                    for sub_attr_name, sub_attr_type in subtype:
                        if sub_attr_type.startswith("i"):
                            o[sub_attr_name] = parse16(body[0:2])
                            body = body[2:]
                        elif sub_attr_type.startswith("s"):
                            slen = int(body[0])
                            o[sub_attr_name] = body[1:slen + 1].decode("utf-8")
                            body = body[slen + 1:]
                        else:
                            raise TypeError("{0} : Unknown type".format(sub_attr_type))
                    l.append(o)
                setattr(self, attr_name, l)

            else:
                raise TypeError("{0} : Unknown type".format(attr_type))

    def dump(self) -> str:
        result = ""
        result += "{0} (0x{1:04X}) from {2}\n".format(self.__class__.__name__, self.typ, self.src)
        for attr_name, attr_type in attr_list:  # type: str, str
            if attr_type.endswith("?") and getattr(self, attr_name) is None:
                continue
            if attr_type.startswith("i"):
                result += "  - {0}(int) : {1}\n".format(attr_name, getattr(self, attr_name))
            elif attr_type.startswith("b"):
                result += "  - {0}(byte) : {1}\n".format(attr_name, getattr(self, attr_name))
            elif attr_type.startswith("s"):
                result += "  - {0}(str) : {1}\n".format(attr_name, getattr(self, attr_name))
            elif attr_type.startswith("[]i"):
                result += "  - {0}([]int) : [{1}]\n".format(
                    attr_name, ", ".join([str(i) for i in getattr(self, attr_name)])
                )
            elif attr_type.startswith("[]{"):
                result += "  - {0}([]{{}}) : {{\n".format(attr_name)
                subtype = [tuple(i.split(":")) for i in attr_type[3:-1].split(";")]
                for i in getattr(self, attr_name):
                    l = list()
                    for sub_attr_name, sub_attr_type in subtype:
                        if sub_attr_type.startswith("i"):
                            l.append((sub_attr_name, i[sub_attr_name]))
                        elif sub_attr_type.startswith("s"):
                            l.append((sub_attr_name, i[sub_attr_name]))
                        else:
                            raise TypeError("{0} : Unknown type".format(sub_attr_type))
                    result += "      " + ", ".join(["{0}: {1}".format(i[0], i[1]) for i in l]) + "\n"
                result += "    }\n"

            else:
                raise TypeError("{0} : Unknown type".format(attr_type))
        return result

    new_class = type(name, (Message,), {
        "__init__": __init__,
        "serialize_body": serialize_body,
        "parse": parse,
        "dump": dump,
    })

    return new_class


def parse_message(data: bytearray) -> Message:
    src = parse16(data[2:4])
    typ = parse16(data[4:6])

    if typ < 0x8000:
        name = message_types[0][0]
        msg_class = globals()[name]
        msg = msg_class(typ=typ, src=src)
    else:
        name = message_types[typ][0]
        msg_class = globals()[name]
        msg = msg_class(src=src)

    msg.parse(data)
    return msg


def init():
    for typ, (name, attributes) in message_types.items():
        globals()[name] = class_factory(name, typ, attributes)

init()
