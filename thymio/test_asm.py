import unittest
import thymio.asm as tasm


class TestAsm(unittest.TestCase):
    def setUp(self):
        pass

    def test_store(self):
        i = tasm.Store(13)
        self.assertEqual(i.emit().hex(), "0d40")

    def test_add(self):
        i = tasm.Add()
        self.assertEqual(i.emit().hex(), "0280")

    def test_beqw(self):
        i = tasm.BranchWhenEqual(30)
        self.assertEqual(i.emit().hex(), "09a11e00")

    def test_beq(self):
        i = tasm.BranchIfEqual(30)
        self.assertEqual(i.emit().hex(), "09a01e00")


if __name__ == '__main__':
    unittest.main()