# Copyright 2017 Jacques Supcik
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ---------------------------------------------------------------------------
# fetch_description.py / JS 17.4.2017
# Download the description of a Thymio-II robot and save the result as
# a JSON file.
# ---------------------------------------------------------------------------
# Thank you very much to the Mobsya organisation for publishing the source
# code of all their products.

import argparse
import hashlib
import json
import queue
import re
import threading

import serial

import thymio.message as tmsg


class ThymioController:

    class Receiver(threading.Thread):
        def __init__(self, ctrl):
            threading.Thread.__init__(self)
            self.ctrl = ctrl

        def run(self):
            while True:
                try:
                    header = self.ctrl.ser.read(6)
                    l = tmsg.parse16(header[0:2])
                    body = self.ctrl.ser.read(l)
                    msg = tmsg.parse_message(header+body)
                    self.ctrl.queue.put(msg)
                except serial.SerialException as e:
                    break

    def __init__(self, url):
        self.ser = serial.serial_for_url(url)
        self.queue = queue.Queue()
        rec = ThymioController.Receiver(self)
        rec.start()

    def sendMessage(self, msg: tmsg.Message):
        self.ser.write(msg.serialize())

    def close(self):
        self.ser.close()


def main():
    parser = argparse.ArgumentParser(description='Download robot description')
    parser.add_argument('--output', help='file name for output')
    parser.add_argument('target', metavar='TARGET', type=str,
                        help='target (such as /dev/cu.usbmodem1411 or socket://localhost:33330)')
    args = parser.parse_args()

    robot = {}

    c = ThymioController(args.target)
    c.sendMessage(tmsg.ListNodes())
    r = c.queue.get()
    if r.__class__.__name__ != "NodePresent":
        raise Exception("Bad message")
    id = r.src

    c.sendMessage(tmsg.GetNodeDescription(dest=id))
    desc = c.queue.get()
    if desc.__class__.__name__ != "Description":
        raise Exception("Bad message")

    robot["name"] = desc.name
    robot["protocol_version"] = desc.protocol_version
    robot["bytecode_size"] = desc.bytecode_size
    robot["stacksize"] = desc.stacksize
    robot["variables_size"] = desc.variables_size
    robot["named_variables"] = list()
    robot["local_events"] = list()
    robot["native_functions"] = list()

    v, e, f = desc.named_variable_size, desc.local_event_size, desc.native_function_size
    while v != 0 or e != 0 or f != 0:
        r = c.queue.get()
        if r.__class__.__name__ == "NamedVariableDescription":
            robot["named_variables"].append({r.name: r.size})
            v -= 1
        elif r.__class__.__name__ == "LocalEventDescription":
            robot["local_events"].append({r.name: r.description})
            e -= 1
        elif r.__class__.__name__ == "NativeFunctionDescription":
            robot["native_functions"].append({r.name: (r.description, [{i["name"]: i["size"]} for i in r.parameter])})
            f -= 1

    c.close()

    j = json.dumps(robot, sort_keys=True)
    h = hashlib.sha256()
    h.update(str.encode(j))
    robot["signature"] = h.hexdigest()

    if args.output is not None:
        file_name = args.output
    else:
        file_name = re.sub(r'\s', '_', desc.name) + ".json"

    with open(file_name, "w") as f:
        json.dump(robot, f, sort_keys=True, indent=2)
        f.close()

if __name__ == '__main__':
    main()