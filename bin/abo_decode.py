# Copyright 2017 Jacques Supcik
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ---------------------------------------------------------------------------
# abo_decode.py / JS 17.4.2017
# ASEBA binary object files (.abo) decoder and disassembler.
# ---------------------------------------------------------------------------
# This program has been written with informations from the ASEBA Studio
# source code. The disassembly part comes from a study of the source code
# of the ASEBA compiler and of the Thymio-II virtual machine.
# Thank you very much to the Mobsya organisation for publishing the source
# code of all their products.

import argparse
import thymio.asm

from thymio.util import *

from typing import BinaryIO

MAGIC = "ABO\0".encode()


def decode(f: BinaryIO):
    # The structure of the object file is described here:
    # https://github.com/aseba-community/aseba/blob/master/clients/studio/MainWindow.cpp#L861
    t = bytearray(f.read(4))
    if t != MAGIC:
        raise Exception("Bad magic.")
    print("Binary format version: {0}".format(parse16(f.read(2))))
    print("Protocol version: {0}".format(parse16(f.read(2))))
    print("Product ID: {0}".format(parse16(f.read(2))))
    print("Firmware version: {0}".format(parse16(f.read(2))))
    print("ID: 0x{0:04X}".format(parse16(f.read(2))))
    print("Name (CRC): 0x{0:04X}".format(parse16(f.read(2))))
    print("Description (CRC): 0x{0:04X}".format(parse16(f.read(2))))
    bytecode_size = parse16(f.read(2))
    bytecode = f.read(2 * bytecode_size)
    print("Bytecode size: {0}".format(bytecode_size))
    print("----- EVENT VECTORS -----")
    ev_size = parse16(bytecode)
    for i in range((ev_size - 1) // 2):
        event_id = s16(parse16(bytecode, 2 + i * 4))
        target = parse16(bytecode, 4 + i * 4)
        print("{0}: {1:04X}".format(event_id, target))
    print("----- BEGIN CODE VECTORS -----")

    pc = ev_size
    for i in thymio.asm.disassembled(bytecode[ev_size * 2:]):
        i.pc = pc
        print("{0:05X}: {1}".format(pc, i))
        pc += 1

    print("----- END CODE (CRC = 0x{0:04X}) -----".format(parse16(f.read(2))))


def main():
    parser = argparse.ArgumentParser(description='Decode ASEBA binary object files (.abo).')
    parser.add_argument('filename', metavar='FILE', type=str, help='name of the object file')
    args = parser.parse_args()

    with open(args.filename, mode="rb") as f:
        decode(f)


if __name__ == '__main__':
    main()
